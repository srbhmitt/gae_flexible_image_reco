FROM gcr.io/google-appengine/python
LABEL python_version=python2.7
RUN virtualenv --no-download /env -p python2.7

# Set virtualenv environment variables. This is equivalent to running
# source /env/bin/activate
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH
ADD requirements.txt /app/
RUN pip install -r requirements.txt
ADD . /app/
RUN apt-get update
RUN apt-get install -y libzbar0
RUN apt-get update && apt-get install -y libsm6 libxext6 libxrender-dev
CMD exec gunicorn -b :$PORT main:app
