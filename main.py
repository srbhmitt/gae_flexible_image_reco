# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# [START app]
import logging
import pyzbar.pyzbar as pyzbar
#from pyzbar.pyzbar import decode
import numpy as np
from PIL import Image

from flask import Flask
from flask import request

from io import BytesIO
import base64
import cv2
import os
import io
import sys
import pickle

from os.path import join


from google.cloud import vision
from google.cloud.vision import types
from googleapiclient import discovery
from oauth2client.client import GoogleCredentials

from difflib import SequenceMatcher
import re

import matplotlib
import matplotlib.pyplot as plt

from difflib import SequenceMatcher
from bs4 import BeautifulSoup
import requests
import re

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./key.json"



app = Flask(__name__)

input_image_path = "./test_pics/sample_10.jpg"
#input_image_path = "./product_pics/not_found.png"
with open(input_image_path, "rb") as image_file:
    encoded_string = base64.b64encode(image_file.read())

query_directory = "./test_pics/"
search_directory = "./product_pics/"

pid_desc_table_path = "./tables/desc_id_table"
pid_image_table_path = "./tables/id_img"
#id_list_path = "./tables/id_list"
#sift_list_path = "./tables/sift_list"
id_list_path = "./tables/id_list_py2.pkl"
sift_list_path = "./tables/sift_list_py2.pkl"



### BARCODE MATCH
def get_barcode(im):
    # Find barcodes and QR codes
    decodedObjects = pyzbar.decode(im)
    return decodedObjects


def match_barcode(barcode, pids, images_path, search_directory):
    product_id = 0
    for obj in barcode:
        product_id = obj.data

    return product_id


### TEXT MATCH
def extract_text_vision(base64_str):
    credentials = GoogleCredentials.get_application_default()
    service = discovery.build('vision', 'v1', credentials=credentials)

    image_content = base64_str
    service_request = service.images().annotate(body={
        'requests': [{
            'image': {
                'content': image_content.decode('UTF-8')
            },
            'features': [{
                'type': 'TEXT_DETECTION',
                'maxResults': 10
            }]
        }]
    })
    texts = service_request.execute()

    # check any texts detected
    if (len(texts['responses'][0]) != 0):
        recog_texts = (texts['responses'][0]['textAnnotations'][0]['description']).split()
        # print(recog_texts)

    else:
        recog_texts = []

    return recog_texts, texts


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def extract_text_category(base64_str):

    credentials = GoogleCredentials.get_application_default()
    service = discovery.build('vision', 'v1', credentials=credentials)

    image_content = base64_str
    service_request = service.images().annotate(body={
        'requests': [{
            'image': {
                'content': image_content.decode('UTF-8')
            },
            'features': [{
                'type': 'LABEL_DETECTION',
                'maxResults': 10
            }]
        }]
    })

    texts = service_request.execute()
    #check any texts detected
    if(len(texts['responses'][0]) != 0):
        category = (texts['responses'][0]['labelAnnotations'][0]['description'])
        confidence = (texts['responses'][0]['labelAnnotations'][0]['score'])

    else:
        category = []
        confidence = 0

    #print(texts)
    return category, confidence

# Beautiful soup
def search_web(category, b64_str):
    page = requests.get("https://mms.mckesson.com/catalog?query=" + category)
    soup = BeautifulSoup(page.content, "html")

    #if not found, return passed in image itself
    not_found = 'No Products Matched Your Criteria'
    product_id = soup.find("h1").text.strip()
    if(product_id == not_found):
        return b64_str,not_found

    #get description
    product_id = ((soup.find("div", {"class" : "row prod-result recommended"})).find('a', href=True)).contents

    #get image
    image_link = ((soup.find("div", {"class" : "row prod-result recommended"})).find('img'))

    response = requests.get(image_link["src"])
    orig_size = Image.open(BytesIO(response.content))

    #resize
    re_size = orig_size.resize((320, 240), Image.ANTIALIAS)

    output = io.BytesIO()
    re_size.save(output, format='JPEG')
    hex_data = output.getvalue()
    #convert to string
    b64_str = base64.b64encode(hex_data)
    return b64_str, product_id

# generate new identifer list
def extract_identifiers(img, expand=False):
    identifiers = []

    recog_texts, texts = extract_text_vision(img)
    x = []
    y = []
    areas = []

    num_texts = len(recog_texts)

    # largest bounding box text
    for index in range(0, num_texts):
        if (len(texts['responses'][0]) != 0):
            text = texts['responses'][0]['textAnnotations'][index]['boundingPoly']['vertices']
            x.append(text[0]['x'])  # x0
            x.append(text[1]['x'])  # x1

            y.append(text[1]['y'])  # y0
            y.append(text[3]['y'])  # y1

            x_diff = x[1] - x[0]
            y_diff = y[1] - y[0]
            x = []
            y = []

        else:
            x_diff = 0
            y_diff = 0
            x = []
            y = []

        area = x_diff * y_diff
        areas.append(area)

    sorted_index = sorted(range(len(areas)), key=lambda i: areas[i])
    top_index = sorted_index[-8:-1]

    for index in top_index:
        identifiers.append(recog_texts[index])
        # print(recog_texts[index])

    # longest word
    lengths = []

    for text in recog_texts:
        length = len(text)
        lengths.append(length)

    sorted_index = sorted(range(len(lengths)), key=lambda i: lengths[i])
    top_index = sorted_index[-5:]

    for index in top_index:
        identifiers.append(recog_texts[index])
        # print(recog_texts[index])

    # single letter L/M/S/XL - can add more size classes
    size_exp = ' '
    size = ' '
    for text in recog_texts:
        text = text.lower()
        match = similar(text, 'medium')
        if match > 0.80:
            size_exp = 'medium'
            size = 'm'
            break;

        match = similar(text, 'm')
        if match == 1.0:
            size = 'm'
            size_exp = 'medium'
            break;

        match = similar(text, 'large')
        if match > 0.80:
            size = 'l'
            size_exp = 'large'
            break;

        match = similar(text, 'l')
        if match == 1.0:
            size = 'l'
            size_exp = 'large'
            break;

        match = similar(text, 'small')
        if match > 0.80:
            size = 's'
            size_exp = 'small'
            break;

        match = similar(text, 's')
        if match == 1.0:
            size_exp = 's'
            size = 'small'
            break;

        match = similar(text, 'xl')
        if match == 1.0:
            size_exp = 'x-large'
            size = 'xl'
            break;

    identifiers = list(set(identifiers))
    identifiers.append(size)
    identifiers.append(size_exp)

    if expand == True:
        print(identifiers)
        print("-------------------------------------------------------------------\n")

    return identifiers


# Combine generated identifiers + provided description tags
def combine_identifiers(desc, images_path, search_directory):
    identifier_list = []
    for img in images_path:
        img = search_directory + img
        base64_str = convert_to_base64(img)

        identifiers = extract_identifiers(base64_str, expand=False)
        identifier_list.append(identifiers)

    new_identifiers = []
    new_id_list = []
    similar_list = []

    for index in range(len(desc)):
        # get words from product desription table
        # strips out special symbols
        # converts to lowercase
        desc_w = re.findall('\w+', desc[index].lower())
        identifier_list[index] = [i.lower() for i in identifier_list[index]]

        for word in desc_w:
            for iden in identifier_list[index]:
                match = similar(word, iden)
                if match > 0.80:
                    similar_list.append(word)
        # remove similar words
        new_identifiers = set(desc_w + identifier_list[index]) - set(similar_list)
        similar_list = []
        new_id_list.append(new_identifiers)

    return new_id_list


# returns product_img and product_id
def match_product_identifier(recog_texts, pids, identifier_list, images_path, search_directory, expand=False):
    num_items = len(pids)
    confidence = []

    # prep before comparing
    # change to lower case
    recog_texts = [i.lower() for i in recog_texts]
    # remove duplicates
    recog_texts = list(set(recog_texts))

    for index in range(len(identifier_list)):
        # change to lower case
        identifier_list[index] = [i.lower() for i in identifier_list[index]]
        found = 0

        matched_words = []

        for rec_word in recog_texts:
            for desc_word in identifier_list[index]:
                match = similar(rec_word, desc_word)
                if match > 0.80:
                    # apply weights to match metric
                    # so longer words matched will have a higher "found" value
                    found = found + len(desc_word)
                    matched_words.append(desc_word)

        if expand == True:
            print(matched_words)
            print("\n")

        if len(recog_texts) > 0:
            confidence.append(found)
        else:
            confidence.append(0.0)

    # return 5 best % match confidence[] with PID
    top_pid = []
    top_confidence = []

    sorted_index = sorted(range(len(confidence)), key=lambda i: confidence[i])
    top_index = sorted_index[-5:]

    for index in top_index:
        top_pid.append(pids[index])
        top_confidence.append(confidence[index])

    confidence = top_confidence[-1]
    if sum(top_confidence) == 0:
        truth_img = "./not_found.png"
        matched_pid = 0
    else:
        top_index = sorted_index[-1]
        matched_pid = pids[top_index]

        #matched_pid = desc[top_index]
        truth_img = search_directory + images_path[top_index]

    return truth_img, matched_pid, confidence

###FLANN MATCH
def compute_all_feature(sift, search_directory):
    # Run only once
    # compute feature vector for 46-image set
    des2={}
    ptr = 0
    for image in images_path:
        fullpath = search_directory + image
        img = plt.imread(fullpath)
        # find the keypoints and descriptors with SIFT
        kp2, des2[ptr] = sift.detectAndCompute(img,None)
        ptr = ptr + 1

    return des2


def image_search(search_directory, images_path,  des1, des2):
    index={}

    ptr = 0
    for image in images_path:
        fullpath = search_directory + image

        # FLANN parameters
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks=50)   # or pass empty dictionary

        flann = cv2.FlannBasedMatcher(index_params,search_params)

        matches = flann.knnMatch(des1,des2[ptr],k=2)

        # Need to draw only good matches, so create a mask
        matchesMask = [[0,0] for i in range(len(matches))]

        features_match = 0
        # ratio test as per Lowe's paper
        for i,(m,n) in enumerate(matches):
            if m.distance < 0.7*n.distance:
                matchesMask[i]=[1,0]
                features_match = features_match + 1

        #print(features_match)

        index[ptr]=features_match
        ptr = ptr + 1
        #print ("total number of photos in this directory: {}".format(len(index)))
    return index


def get_matchimage(index, pids, images_path, search_directory, FLANN_RATIO_THRESH, FLANN_FEATURES_THRESH):

    # get top two feature matches
    sorted_index = sorted(range(len(index)), key=lambda i:index[i])
    confidences = sorted_index[-2:]

    # ratio test
    second_closest_match = index[confidences[0]]
    best_match = index[confidences[1]]

    # high confidence
    if(best_match > FLANN_RATIO_THRESH * second_closest_match):
        if(best_match > FLANN_FEATURES_THRESH):
            confidence = 1
        else:
            confidence = 0

    # low confidence
    else:
        confidence = 0

    #print("ratio: {}, features matched: {}" .format(best_match/second_closest_match, best_match))

    match_pid = pids[confidences[1]]
    best_match_img_path = search_directory + images_path[confidences[1]]

    return best_match_img_path, match_pid, confidence


###PRODUCT CLASSIFICATION

#load id - image table from id_img file
#used for visualizing the matched product
def load_pid_image_table(path, expand=False):
    images_path = []
    pids = []

    # read 1st 3 digits from each line into ids and the rest as tags
    with open(path) as fp:
        for line in fp:
            ids  = line[0:3]
            imgs = line[4:].strip('\n')
            pids.append(ids)
            images_path.append(imgs)

    print("Image paths loaded: {}" .format(len(images_path)))
    print("Product IDs loaded: {}" .format(len(pids)))

    return pids, images_path

#read list from file desc_id_table
#Takes in table file path
#return pid and description tags

def load_pid_desc_table(path, expand=False):
    pids = [] # 46 ids
    desc = []# 46 strings

    # read 1st 3 digits from each line into ids and the rest as tags
    with open(path) as fp:
        for line in fp:
            ids  = line[0:3]
            tags = line[4:]
            pids.append(ids)
            desc.append(tags)

    print("Description Tags loaded: {}" .format(len(desc)))
    print("Product IDs loaded: {}" .format(len(pids)))

    if expand==True:
        print("\n Tags:\n")

    return pids, desc

def convert_to_base64(input_image_path):
    image = open(input_image_path, 'rb') #open binary file in read mode
    string = base64.b64encode(image.read())
    return string

def convert_to_image(string):
    image = Image.open(io.BytesIO(base64.b64decode(string)))
    return image

def find_product(base64_str, new_id_list, sift, des2, pids,images_path, VISION_THRESH, FLANN_RATIO_THRESH,
                 FLANN_FEATURES_THRESH, CATEGORY_THRESH):

    #convert base64 to image
    query_image = convert_to_image(base64_str)
    print("<1> reading barcode")
    #priority1: barcode reader
    barcode = get_barcode(query_image)

    #match barcode with product_img, product_id
    if(len(barcode) != 0):
        product_id = match_barcode(barcode, pids, images_path, search_directory)
         # return same query image and barcode
        re_size = query_image.resize((320, 240), Image.ANTIALIAS)
        output = io.BytesIO()
        re_size.save(output, format='JPEG')
        hex_data = output.getvalue()
        b64_str = base64_str = base64.b64encode(hex_data)
        return b64_str, product_id

    #no code found
    else:
        print("no code found")
        print("<2> matching extracted text")
        #priority2: vision api
        recog_texts, texts = extract_text_vision(base64_str)
        product_img, product_id, vision_confidence = match_product_identifier(recog_texts, pids, new_id_list,
                                                            images_path, search_directory, expand=False)

        #high confidence
        if(vision_confidence > VISION_THRESH):
            #match product_img, product_id
            product_img = product_img
            product_id = product_id

        #low confidence
        else:
            print('low vision confidence')
            print("<3> matching sift features")
            #priority3: flann matcher
            npimg = np.array(query_image)
            kp1, des1 = sift.detectAndCompute(npimg,None)
            index = image_search(search_directory, images_path, des1, des2)
            product_img, product_id, flann_confidence = get_matchimage(index, pids, images_path, search_directory,
                                                                       FLANN_RATIO_THRESH, FLANN_FEATURES_THRESH)

            #high confidence
            if(flann_confidence == 1):
                #match product_img, product_id
                product_img = product_img
                product_id = product_id

            #low confidence
            else:
                print('low flann confidence')
                print('<4> getting product category')
                # return same query image and product category
                re_size = query_image.resize((320, 240), Image.ANTIALIAS)
                output = io.BytesIO()
                re_size.save(output, format='JPEG')
                hex_data = output.getvalue()
                b64_str = base64_str = base64.b64encode(hex_data)

                product_id, confidence = extract_text_category(base64_str)

                if(confidence > CATEGORY_THRESH):
                    print('<5> searching mckesson webpage for "{}"'.format(product_id))
                    b64_str, product_id = search_web(product_id, b64_str)
                    return b64_str, product_id

                else:
                    print('low category confidence')
                    error_match = "no matching product! please type your search"
                    return b64_str, error_match


    #resize
    base64_en_img = resize_img(product_img)
    product_id = str(product_id)

    return base64_en_img, product_id


#resize image, inp - image path, op - b64 string
def resize_img(input_image_path):
    orig_size = Image.open(input_image_path)
    re_size = orig_size.resize((320, 240), Image.ANTIALIAS)

    output = io.BytesIO()
    re_size.save(output, format='JPEG')
    hex_data = output.getvalue()
    #convert to string
    base64_str = base64.b64encode(hex_data)
    return base64_str

# match image with one of 46 product pics in search_direcory
# product_img -> base64 encoded string
# product_id  -> string
def run_code_app(base64_str):
    # Load tables
    pids, images_path = load_pid_image_table(pid_image_table_path, expand=False)
    pids, desc = load_pid_desc_table(pid_desc_table_path, expand=False)

    # Set vision_confidence threshold
    VISION_THRESH = 25

    # Set flann confidence threshold
    FLANN_RATIO_THRESH = 1.5
    FLANN_FEATURES_THRESH = 100

    # Set vision category threshold
    CATEGORY_THRESH = 0.80

    print("generating identifiers")
    # read identifier list
    with open(id_list_path, 'rb') as fp:
        new_id_list = pickle.load(fp)

    # Initiate SIFT detector
    sift = cv2.xfeatures2d.SIFT_create()

    print("generating sift features")
    # read sift features for all 46 images
    with open(sift_list_path, 'rb') as fp:
        des2 = pickle.load(fp)

    product_img, product_id = find_product(base64_str, new_id_list, sift, des2, pids, images_path,
                                           VISION_THRESH, FLANN_RATIO_THRESH, FLANN_FEATURES_THRESH,CATEGORY_THRESH)

    #convert base64 encoded string back to image to display
    #product_img = convert_to_image(product_img)

    return product_img, product_id





#to here





def decode(im) :
    # Find barcodes and QR codes
    decodedObjects = pyzbar.decode(im)
    return decodedObjects


def convert_base64(string):
    im = Image.open(BytesIO(base64.b64decode(string)))
    return im


def barcode_scanner(string):
    image_64_decoded = convert_base64(string)
    barcode_string = decode(image_64_decoded)
    return barcode_string


@app.route('/', methods = ['GET', 'POST', 'DELETE'])
def hello():
    if request.method == 'GET':
        """Return a friendly HTTP greeting."""
        img, prod_id = run_code_app(encoded_string)
        if isinstance(prod_id, (list,)):
            return (prod_id[0]+"base64XXX"+img)
        else:
            return (prod_id+"base64XXX"+img)
        #return ("abc")

    if request.method == 'POST':
        a=request.form.get('name').replace("-","+")
        b=a.replace("_","/")
        img, prod_id = run_code_app(b)
        if isinstance(prod_id, (list,)):
            return (prod_id[0]+"base64XXX"+img)
        else:
            return (prod_id+"base64XXX"+img)


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END app]
